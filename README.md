# Analyse de données en Python
 
# Objectif Général du cours
- Savoir utiliser les principaux outils de traitement et d'analyse de données pour Python
- Être capable d'extraire des données d'un fichier et les manipuler
- Apprendre à mettre en place un modèle d'apprentissage simple

**Contenu**
- Mettre en place un environnement de travail Python
- Manipuler les fonctions et objets fondamentaux
- Structurer le code au sein d’un projet
- Utiliser des librairies Python spécialisées
1. le langage Python
2. Specialement pour la Data Science
3. Stockage de donées
4. Manipulation des données
5. Outils pour l'analyse de donées

